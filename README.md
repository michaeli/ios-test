## iOsProgramming Task

In order to be considered for the iOS developer position, you mush complete the following steps. 

*Note: This task should take no longer than 1-2 hours at the most.*



### Prerequisites

- Please note that this will require some basic Objective-C and iOS knowledge. 

## Task

1. Fork this repository (if you don't know how to do that, Google is your friend)
2. Create a *source* folder to contain your code. 
3. In the *source* directory, please create an iPhone app for iOS7 that accomplishes the following:
	- Connect to the [Github API](http://developer.github.com/)
	- Find the [rails/rails](http://github.com/rails/rails) repository
	- Find the most recent commits (choose at least 25 or more of the commits)
	- Create a ListView that groups the recent commits by author in a custom view. 
	- [Click here](https://bitbucket.org/alex-touchnote/ios-test/src/16f2d7af018482ebac5fd1515b9cccbd519ba8c7/mockup.jpg?at=master) for a screenshot mock up of what the final product should look like
4. Commit and Push your code to your new repository
5. Send us a pull request, we will review your code and get back to you


